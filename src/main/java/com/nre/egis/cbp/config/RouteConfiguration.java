package com.nre.egis.cbp.config;

import javax.inject.Inject;

import org.apache.camel.component.kafka.KafkaEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nre.egis.cbp.property.KafkaProperties;


@Configuration
@EnableConfigurationProperties({ KafkaProperties.class })
public class RouteConfiguration {
	@Inject
	private KafkaProperties KafkaProperties;
	
	@Value("${egis.from.dos-topic}")
	private String fromDosTopic;
	
	@Value("${egis.from.external-topic}")
	private String fromExternalTopic;
	
	@Value("${egis.to.audit-topic}")
	private String toAuditTopic;
	
	@Value("${egis.to.camino-topic}")
	private String toCaminoTopic;
	
    @Bean
    public KafkaEndpoint fromKafkaDosEndpoint() {
        return fromKafkaEndpoint(fromDosTopic);
    }

    @Bean
    public KafkaEndpoint fromKafkaExternalEndpoint() {
        return fromKafkaEndpoint(fromExternalTopic);
    }
    
    @Bean
    public KafkaEndpoint toKafkaDosEndpoint() {
        return toKafkaEndpoint(fromDosTopic);
    }

    @Bean
    public KafkaEndpoint toKafkaCaminoEndpoint() {
        return toKafkaEndpoint(toCaminoTopic);
    }
    
    @Bean
    public KafkaEndpoint toKafkaCbpEndpoint() {
        return toKafkaEndpoint(fromExternalTopic);
    }
    
    @Bean
    public KafkaEndpoint toKafkaAuditEndpoint() {
        return toKafkaEndpoint(toAuditTopic);
    }

//  @Bean(name = "kafkaBackgroundCheckResult")
//  public DefaultProducer tokafkaCaminoProducer() {
//      return toKafkaProducer(toCaminoTopic);
//  }
//    
//    @Bean(name = "kafkaBackgroundCheckRequest")
//    public DefaultProducer toKafkaCbpProducer() {
//        return toKafkaProducer(fromExternalTopic);
//    }
//
//    @Bean
//    public DefaultProducer toKafkaAuditProducer() {
//        return toKafkaProducer(toAuditTopic);
//    }

    private KafkaEndpoint fromKafkaEndpoint(String topic) {
        KafkaEndpoint endpoint = baseKafkaEndpoint(topic);
        endpoint.getConfiguration().setAutoOffsetReset(KafkaProperties.getConsumer().getAutoOffsetReset());
        endpoint.getConfiguration().setGroupId(KafkaProperties.getConsumer().getGroupId());
        endpoint.getConfiguration().setKeyDeserializer(KafkaProperties.getConsumer().getKeyDeserializer());
        endpoint.getConfiguration().setValueDeserializer(KafkaProperties.getConsumer().getValueDeserializer());
        return endpoint;
    }

    private KafkaEndpoint toKafkaEndpoint(String topic) {
        KafkaEndpoint endpoint = baseKafkaEndpoint(topic);
        endpoint.getConfiguration().setKeySerializerClass(KafkaProperties.getProducer().getKeySerializer());
        endpoint.getConfiguration().setSerializerClass(KafkaProperties.getProducer().getValueSerializer());
        endpoint.getConfiguration().setLingerMs(KafkaProperties.getProducer().getLingerMs());
        return endpoint;
    }

    private KafkaEndpoint baseKafkaEndpoint(String topic) {
        String endpointUri = "kafka:" + KafkaProperties.getBootstrapServers().iterator().next();
        KafkaEndpoint endpoint = new DefaultCamelContext().getEndpoint(endpointUri, KafkaEndpoint.class);
        endpoint.getConfiguration().setBrokers(KafkaProperties.getBootstrapServers().iterator().next());
        endpoint.getConfiguration().setTopic(topic);
        return endpoint;
    }
}
