package com.nre.egis.cbp.bean;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wrapsnet.xmlschema.ApplicantType;
import org.wrapsnet.xmlschema.WRAPSSubmission;

import com.nre.egis.cbp.util.RandomUtil;
import wraps.atsp.cbp.dhs.gov._1.AtspResponse;
import wraps.atsp.cbp.dhs.gov._1.HeaderType;
import wraps.atsp.cbp.dhs.gov._1.KfeHitType;
import wraps.atsp.cbp.dhs.gov._1.KfeMessageCodeType;
import wraps.atsp.cbp.dhs.gov._1.ResponseType;

public class BackgroundCheckBean {
    private static final Logger logger = LoggerFactory.getLogger(BackgroundCheckBean.class);

    public AtspResponse mockExternalResponse(@Body WRAPSSubmission body, Exchange exchange) {
        try {
            AtspResponse atspResponse = new AtspResponse();
            HeaderType header = new HeaderType();
            KfeHitType kfeHitType = new KfeHitType();

            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
            XMLGregorianCalendar currentDateTime = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);

            ApplicantType applicant = body.getApplicant().get(0);
            String requestId = applicant.getApplicantIACRequest().getIACRequestID();

            header.setRequestId(requestId);
            header.setRequestPersonId(applicant.getPersonID());
			header.setResponseId(RandomUtil.generateNumericId());
            header.setResponseTime(currentDateTime);

            kfeHitType.setMessageType(KfeMessageCodeType.INITIAL);
            kfeHitType.setResultType(ResponseType.GREEN);

            atspResponse.setHeader(header);
            atspResponse.setKfeHit(kfeHitType);

            exchange.getIn().setHeader("request-id", requestId);

            logger.debug("+++++++CBP ATSPResponse Object: " + atspResponse.toString());

            return atspResponse;
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException("Error converting XML to object", e);
        }
    }
}
