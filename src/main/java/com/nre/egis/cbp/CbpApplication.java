package com.nre.egis.cbp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath:spring/camel-context.xml"})
public class CbpApplication {

    public static void main(String[] args) {
        SpringApplication.run(CbpApplication.class, args);
    }

}