/**
 * 
 */
package com.nre.egis.cbp.util;

import org.apache.commons.lang3.RandomStringUtils;

public final class RandomUtil {
    private static final int DEF_COUNT = 8;

    private RandomUtil() {}

    public static String generateNumericId() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }
}
