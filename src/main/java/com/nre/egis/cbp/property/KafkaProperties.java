package com.nre.egis.cbp.property;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.kafka")
public class KafkaProperties {
	private List<String> bootstrapServers = Arrays.asList("localhost:9092");
	private Consumer consumer;
	private Producer producer;

	public List<String> getBootstrapServers() {
		return bootstrapServers;
	}

	public void setBootstrapServers(List<String> bootstrapServers) {
		this.bootstrapServers = bootstrapServers;
	}

	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public static class Consumer {
		private String autoOffsetReset;
		private String groupId;
		private String keyDeserializer;
		private String valueDeserializer;

		public String getAutoOffsetReset() {
			return autoOffsetReset;
		}

		public void setAutoOffsetReset(String autoOffsetReset) {
			this.autoOffsetReset = autoOffsetReset;
		}

		public String getGroupId() {
			return groupId;
		}

		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}

		public String getKeyDeserializer() {
			return keyDeserializer;
		}

		public void setKeyDeserializer(String keyDeserializer) {
			this.keyDeserializer = keyDeserializer;
		}

		public String getValueDeserializer() {
			return valueDeserializer;
		}

		public void setValueDeserializer(String valueDeserializer) {
			this.valueDeserializer = valueDeserializer;
		}
	}

	public static class Producer {
		private String keySerializer;
		private String valueSerializer;
		private Integer lingerMs;

		public String getKeySerializer() {
			return keySerializer;
		}

		public void setKeySerializer(String keySerializer) {
			this.keySerializer = keySerializer;
		}

		public String getValueSerializer() {
			return valueSerializer;
		}

		public void setValueSerializer(String valueSerializer) {
			this.valueSerializer = valueSerializer;
		}

		public Integer getLingerMs() {
			return lingerMs;
		}

		public void setLingerMs(Integer lingerMs) {
			this.lingerMs = lingerMs;
		}
	}
}
